﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Kcknap
{
    class TCPHandler
    {
        Thread t;
        String ip = "";

        public TCPHandler()
        {
            try
            {
                ip = new StreamReader("ip.txt").ReadLine();
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex);
            }

            t = new Thread(HandleIncomingPackets);
            t.Start();
        }


        public void HandleIncomingPackets()
        {
            Console.WriteLine("TCP listening");
            try
            {
                IPAddress adresa = IPAddress.Parse(ip);
                int port = 80;
                TcpListener listener = new TcpListener(adresa, port);
                listener.Start();
                while (true)
                {
                    String movements = "15,0\n" +
                                       "0,0\n" +
                                       "15,0\n" +
                                       "0,0\n" +
                                       "15,0\n" +
                                       "0,0\n"
                        ;

                    TcpClient klijent = listener.AcceptTcpClient();
                    NetworkStream stream = klijent.GetStream();

                    StreamReader reader = new StreamReader(stream);
                    string data = "";
                    string msg = "";
                    //if(stream.DataAvailable)
                    {
                        while (!String.IsNullOrEmpty((data = reader.ReadLine())))
                        {
                            msg = msg + data + "\n";
                        }
                    }
                    Console.WriteLine(msg);
                    string[] command = msg.Split('\n');
                    if (command[0].Contains("GET"))
                    {
                        String mathchId = "";
                        String playerID = "";
                        bool mivagyunk = true;
                        try
                        {
                            //GET /?match=14&player=1 HTTP/1.1
                            Console.WriteLine(command[0]);
                            int fi = command[0].IndexOf('=');
                            int sc = command[0].IndexOf('&');

                            mathchId = command[0].Substring(fi + 1, sc - (fi + 1));
                            if (!UDPDataMonitor.matchek.Contains(int.Parse(mathchId)))
                            {
                                UDPDataMonitor.matchek.Add(int.Parse(mathchId));
                            }
                            for (int k = 0; k < UDPDataMonitor.matchek.Count; k++)
                            {
                                Console.WriteLine("++:" + UDPDataMonitor.matchek[k]);
                            }
                            command[0] = command[0].Substring(sc);
                            fi = command[0].IndexOf('=');
                            sc = command[0].IndexOf(' ');

                            playerID = command[0].Substring(fi + 1, sc - (fi + 1));
                            if (playerID.Equals("2"))
                            {
                                mivagyunk = false;
                            }
                            if (!mivagyunk)
                            {
                                movements = "-15,0\n" +
                                            "0,0\n" +
                                            "-15,0\n" +
                                            "0,0\n" +
                                            "-15,0\n" +
                                            "0,0\n"
                                    ;
                            }

                            Console.WriteLine("Count: " + MatchList.matchList.Count);
                            for (int i = 0; i < MatchList.matchList.Count; i++)
                            {
                                Console.WriteLine("-----");
                                for (int kIndex = 0; kIndex < MatchList.matchList.Count; kIndex++)
                                {
                                    Console.WriteLine("--: " + MatchList.matchList[kIndex].MatchId);
                                }
                                if (MatchList.matchList[i].MatchId == int.Parse(mathchId))
                                {
                                    if (!mivagyunk)
                                    {
                                        MatchList.matchList[i].swapPlayers();
                                    }
                                    //MI lépünk

                                    List<int> idk = new List<int>();
                                    string[] movementsList = new string[3];

                                    idk.Add(0);
                                    idk.Add(1);
                                    idk.Add(2);


                                    if (MooveVariations.MindenkiKozelE(i))
                                    {
                                        movements = "";
                                        int legk = MooveVariations.GetLegkozelebbi(i);
                                        idk.Remove(legk);

                                        movementsList[legk] = MatchList.matchList[i].player1[legk]
                                            .MoveToLabda(MatchList.matchList[i].labda, mivagyunk);

                                        for (int j = 0; j < idk.Count; j++)
                                        {
                                            movementsList[idk[j]] = MatchList.matchList[i].player1[idk[j]]
                                                .MoveToEnemy(MatchList.matchList[i].player2[0]);
                                        }
                                    }

                                    else
                                    {
                                        Console.WriteLine("Labdahoz");
                                        movements = "";


                                        if (MatchList.matchList[i].labda.X > 900)
                                        {
                                            movementsList[0] = MatchList.matchList[i].player1[2].Stay();
                                            movementsList[1] = MatchList.matchList[i].player1[2].Stay();
                                            movementsList[2] = MatchList.matchList[i].player1[2]
                                                .MoveToLabdaMoge(MatchList.matchList[i].labda, mivagyunk);
                                        }
                                        else
                                        {
                                            for (int j = 0; j < idk.Count; j++)
                                            {
                                                movementsList[j] = MatchList.matchList[i].player1[j]
                                                    .MoveToLabdaMoge(MatchList.matchList[i].labda, mivagyunk);
                                            }
                                        }
                                    }


                                    if (MooveVariations.JobbAlsoSarok(i))
                                    {
                                        int legk = MooveVariations.GetLegkozelebbi(i);
                                        idk.Remove(legk);

                                        movementsList[legk] = MatchList.matchList[i].player1[legk]
                                            .MoveToLabda(MatchList.matchList[i].labda, mivagyunk);

                                        for (int j = 0; j < idk.Count; j++)
                                        {
                                            movementsList[idk[j]] = MatchList.matchList[i].player1[idk[j]]
                                                .Moove(-15, -15);
                                        }
                                    }
                                    if (MooveVariations.BalAlsoSarok(i))
                                    {
                                        int legk = MooveVariations.GetLegkozelebbi(i);
                                        idk.Remove(legk);

                                        movementsList[legk] = MatchList.matchList[i].player1[legk]
                                            .MoveToLabda(MatchList.matchList[i].labda, mivagyunk);

                                        for (int j = 0; j < idk.Count; j++)
                                        {
                                            movementsList[idk[j]] = MatchList.matchList[i].player1[idk[j]]
                                                .Moove(15, 15);
                                        }
                                    }
                                    if (MooveVariations.JobbFelsoSarok(i))
                                    {
                                        int legk = MooveVariations.GetLegkozelebbi(i);
                                        idk.Remove(legk);

                                        movementsList[legk] = MatchList.matchList[i].player1[legk]
                                            .MoveToLabda(MatchList.matchList[i].labda, mivagyunk);

                                        for (int j = 0; j < idk.Count; j++)
                                        {
                                            movementsList[idk[j]] = MatchList.matchList[i].player1[idk[j]]
                                                .Moove(-15, 15);
                                        }
                                    }
                                    if (MooveVariations.BalFelsoSarok(i))
                                    {
                                        int legk = MooveVariations.GetLegkozelebbi(i);
                                        idk.Remove(legk);

                                        movementsList[legk] = MatchList.matchList[i].player1[legk]
                                            .MoveToLabda(MatchList.matchList[i].labda, mivagyunk);

                                        for (int j = 0; j < idk.Count; j++)
                                        {
                                            movementsList[idk[j]] = MatchList.matchList[i].player1[idk[j]]
                                                .Moove(-15, 15);
                                        }
                                    }


//                                    if (MooveVariations.Invasion(i, mivagyunk))
//                                    {
//                                        for (int j = 0; j < MatchList.matchList[i].player1.Length; j++)
//                                        {
//                                            movementsList[j] = MatchList.matchList[i].player1[j]
//                                                .MooveHelyre(MatchList.matchList[i].labda, mivagyunk, j);
//                                        }
//                                    }

                                    for (int j = 0; j < movementsList.Length; j++)
                                    {
                                        movements += movementsList[j] + "\n";
                                    }
                                    movements = movements.Substring(0, movements.LastIndexOf("\n") - 1);
                                    break;
                                }
                            }
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e);
                        }
                    }
                    movements = movements.Replace('.', ',');
                    Console.WriteLine(movements);
                    byte[] bytesFile = Encoding.Default.GetBytes(movements);

                    string response = "HTTP/1.1 200 OK\r\n"
                                      + "Server: PMFST\r\n"
                                      + $"Content-Length: {bytesFile.Length}\r\n"
                                      + "Content-Type: text/html\r\n"
                                      + "Connection: Closed\r\n"
                                      + "\r\n";

                    byte[] bytesResponse = Encoding.ASCII.GetBytes(response);

                    stream.Write(bytesResponse, 0, bytesResponse.Length);
                    stream.Write(bytesFile, 0, bytesFile.Length);
                    stream.Flush();
                    stream.Dispose();
                    klijent.Close();
                    Console.WriteLine("Kész");
                }
            }
            catch (SocketException e)
            {
                Console.WriteLine("SocketException: {0}", e);
            }
        }
    }
}