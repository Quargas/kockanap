﻿using System;
using System.Linq;

namespace Kcknap
{
    class Jatekos
    {
        float x;
        float y;
        float size = 0;

        public float X
        {
            get { return x; }
            set { x = value; }
        }

        public float Y
        {
            get { return y; }
            set { y = value; }
        }

        public String MoveToLabda(Labda labda, bool mivagyunk)
        {
            if (mivagyunk)
            {
                if (this.x < labda.X)
                {
                    if (this.y > labda.Y)
                    {
                        float x = (this.x - labda.X) * 10;
                        float y = (this.y - (labda.Y + 15) * 10);
                    }
                    if (this.y > labda.Y)
                    {
                        float x = (this.x - labda.X) * 10;
                        float y = (this.y - (labda.Y + 15) * 10);
                    }
                }
                else
                {
                    return MoveToLabdaMoge(labda, mivagyunk);
                }
            }
            else
            {
                if (this.y > labda.Y)
                {
                    float x = (this.x - labda.X) * 10;
                    float y = (this.y - labda.Y - 3) * 10;
                }
                if (this.y > labda.Y)
                {
                    float x = (this.x - labda.X) * 10;
                    float y = (this.y - labda.Y + 3) * 10;
                }
                else
                {
                    return MoveToLabdaMoge(labda, mivagyunk);
                }
            }
            return Moove(x, y);
        }


        public String MoveToEnemy(Jatekos jatekos)
        {
            float x = jatekos.X - this.x - 31;
            float y = jatekos.Y - this.y + 31;

            return Moove(x, y);
        }

        public String MoveKozepre()
        {
            float x = 500 - this.x + 60;
            float y = 350 - this.y + 60;

            return Moove(x, y);
        }

        Random rnd = new Random();

        public String MoveToLabdaMoge(Labda labda, bool mivagyunk)
        {
            int atmegye = rnd.Next(1, 3);

            if (mivagyunk)
            {
                if (this.x <= 550 && this.x >= 450 && atmegye % 2 == 0)
                {
                    return Moove(15, 0);
                }
                float x = labda.X - this.x - 45;
                float y = labda.Y - this.y + 45;

                if (this.x > labda.X)
                {
                    if (this.y > labda.Y)
                    {
                        x = labda.X - this.x - 45;
                        y = labda.Y - this.y + 45;
                    }
                    else if (this.y < labda.Y)
                    {
                        x = labda.X - this.x + 45;
                        y = labda.Y - this.y - 45;
                    }
                }


                return Moove(x, y);
            }
            else
            {
                if (this.x >= 550 && this.x <= 450 && atmegye % 2 == 0)
                {
                    return Moove(-15, 0);
                }
                float x = labda.X - this.x - 45;
                float y = labda.Y - this.y + 45;

                if (this.x < labda.X)
                {
                    if (this.y < labda.Y)
                    {
                        x = labda.X - this.x - 45;
                        y = labda.Y - this.y + 45;
                    }
                    else if (this.y > labda.Y)
                    {
                        x = labda.X - this.x + 45;
                        y = labda.Y - this.y - 45;
                    }
                }

                return Moove(x, y);
            }
        }

        public String Stay()
        {
            return Moove(0, 0);
        }


        public string MooveHelyre(Labda labda, bool mivagyunk, int id)
        {
            if (this.x == 60 && this.y == 25 * id)
            {
                MoveToLabda(labda, mivagyunk);
            }
            return Moove(60, 25 * id);
        }

        public String Moove(float x, float y)
        {
            return x + "\n" + y;
        }
    }
}