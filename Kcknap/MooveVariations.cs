﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kcknap
{
    class MooveVariations
    {
        public static string DefaultMooveMent(int matchID)
        {
            String movements = "";

//            movements = MatchList.matchList[matchID].player1[0]
//                            .MoveToLabda(MatchList.matchList[matchID].labda) + "\n";
//            movements += MatchList.matchList[matchID].player1[1]
//                             .MoveToLabda(MatchList.matchList[matchID].labda) + "\n";
//            movements += MatchList.matchList[matchID].player1[2]
//                             .MoveToLabda(MatchList.matchList[matchID].labda) + "\n";

            return movements;
        }


        public static int GetLegkozelebbi(int matchID)
        {
            Jatekos player1 = MatchList.matchList[matchID].player1[0];
            Jatekos player2 = MatchList.matchList[matchID].player1[1];
            Jatekos player3 = MatchList.matchList[matchID].player1[2];
            Labda labda = MatchList.matchList[matchID].labda;
            double[] tavolsagok = new double[3];

            tavolsagok[0] = Math.Sqrt(Math.Pow(player1.X - labda.X, 2) + Math.Pow(player1.Y - labda.Y, 2));
            tavolsagok[1] = Math.Sqrt(Math.Pow(player2.X - labda.X, 2) + Math.Pow(player2.Y - labda.Y, 2));
            tavolsagok[2] = Math.Sqrt(Math.Pow(player3.X - labda.X, 2) + Math.Pow(player3.Y - labda.Y, 2));


            Console.WriteLine("tav: " + tavolsagok[0]);
            Console.WriteLine("tav: " + tavolsagok[1]);
            Console.WriteLine("tav: " + tavolsagok[2]);

            int minIndex = Array.IndexOf(tavolsagok, tavolsagok.Min());


            return minIndex;
        }


        public static bool MindenkiKozelE(int matchID)
        {
            Jatekos player1 = MatchList.matchList[matchID].player1[0];
            Jatekos player2 = MatchList.matchList[matchID].player1[1];
            Jatekos player3 = MatchList.matchList[matchID].player1[2];
            Labda labda = MatchList.matchList[matchID].labda;
            double[] tavolsagok = new double[3];
            tavolsagok[0] = Math.Sqrt(Math.Pow(player1.X - labda.X, 2) + Math.Pow(player1.Y - labda.Y, 2));
            tavolsagok[1] = Math.Sqrt(Math.Pow(player2.X - labda.X, 2) + Math.Pow(player2.Y - labda.Y, 2));
            tavolsagok[2] = Math.Sqrt(Math.Pow(player3.X - labda.X, 2) + Math.Pow(player3.Y - labda.Y, 2));

            if (tavolsagok[0] <= 150 && tavolsagok[1] <= 150 && tavolsagok[2] <= 150)
            {
                return true;
            }

            else
            {
                return false;
            }
        }

        public static bool JobbAlsoSarok(int i)
        {
            return !MooveVariations.MindenkiKozelE(i) &&
                   (MatchList.matchList[i].player1[0].X > 900 &&
                    MatchList.matchList[i].player1[0].Y > 600) &&
                   (MatchList.matchList[i].player1[1].X > 900 &&
                    MatchList.matchList[i].player1[1].Y > 600) &&
                   (MatchList.matchList[i].player1[2].X > 900 &&
                    MatchList.matchList[i].player1[2].Y > 600);
        }

        public static bool BalAlsoSarok(int i)
        {
            return !MooveVariations.MindenkiKozelE(i) &&
                   (MatchList.matchList[i].player1[0].X < 100 &&
                    MatchList.matchList[i].player1[0].Y > 600) &&
                   (MatchList.matchList[i].player1[1].X < 900 &&
                    MatchList.matchList[i].player1[1].Y > 600) &&
                   (MatchList.matchList[i].player1[2].X < 900 &&
                    MatchList.matchList[i].player1[2].Y > 600);
        }

        public static bool JobbFelsoSarok(int i)
        {
            return !MooveVariations.MindenkiKozelE(i) &&
                   (MatchList.matchList[i].player1[0].X > 900 &&
                    MatchList.matchList[i].player1[0].Y < 200) &&
                   (MatchList.matchList[i].player1[1].X > 900 &&
                    MatchList.matchList[i].player1[1].Y < 200) &&
                   (MatchList.matchList[i].player1[2].X > 900 &&
                    MatchList.matchList[i].player1[2].Y < 200);
        }

        public static bool BalFelsoSarok(int i)
        {
            return !MooveVariations.MindenkiKozelE(i) &&
                   (MatchList.matchList[i].player1[0].X < 100 &&
                    MatchList.matchList[i].player1[0].Y < 200) &&
                   (MatchList.matchList[i].player1[1].X < 900 &&
                    MatchList.matchList[i].player1[1].Y < 200) &&
                   (MatchList.matchList[i].player1[2].X < 900 &&
                    MatchList.matchList[i].player1[2].Y < 200);
        }

        public static bool Invasion(int id, bool mivagyunk)
        {
            return enemy1Behind(id, mivagyunk) && enemy2Behind(id, mivagyunk) && enemy3Behind(id, mivagyunk);
        }

        private static bool enemy1Behind(int id, bool mivagyunk)
        {
            Jatekos player1 = MatchList.matchList[id].player2[0];
            if (mivagyunk)
            {
                return player1.X <= 500;
            }

            return player1.X >= 500;
        }

        private static bool enemy2Behind(int id, bool mivagyunk)
        {
            Jatekos player1 = MatchList.matchList[id].player2[1];
            if (mivagyunk)
            {
                return player1.X <= 500;
            }

            return player1.X >= 500;
        }

        private static bool enemy3Behind(int id, bool mivagyunk)
        {
            Jatekos player1 = MatchList.matchList[id].player2[2];
            if (mivagyunk)
            {
                return player1.X <= 500;
            }

            return player1.X >= 500;
        }
    }
}