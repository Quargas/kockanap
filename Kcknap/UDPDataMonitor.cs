﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace Kcknap
{
    class UDPDataMonitor
    {    public static List<int> matchek = new List<int>();
        UdpClient udpClient = new UdpClient(6996);

        Thread t;
        IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);

        public UDPDataMonitor()
        {
            t = new Thread(HandleIncomingPackets);
            t.Start();
        }


        private void HandleIncomingPackets()
        {
            Console.WriteLine("TCP listener started");
            string returnData = "";

            while (!returnData.Equals("$ß$$ß$ßß$$ß$ß$ßß$ß$"))
            {
                StreamWriter streamWriter = new StreamWriter("output.txt", true);

                Byte[] receiveBytes = udpClient.Receive(ref RemoteIpEndPoint);
                returnData = Encoding.ASCII.GetString(receiveBytes);
                
                for (int i = 0; i < matchek.Count; i++)
                {
                    ProcessInput(matchek[i],returnData);
                }
                
                streamWriter.WriteLine(returnData);
                streamWriter.Flush();
                streamWriter.Close();
            }
        }

        public void ProcessInput(int id, String text)
        {
            
            text = text.Replace("????", "\n");

            string[] data = text.Split('\n');

            for (int i = 0; i < data.Length; i++)
            {
                data[i] = data[i].Replace(',', '.');
                String[] sor_ = data[i].Split('|');

                if ((sor_[0]) == id + "" && sor_.Length == 16)
                {
                    Match match = new Match();
                    int j = 0;
                    bool van = false;
                    while (MatchList.matchList.Count > j)
                    {
                        if (MatchList.matchList[j].MatchId == id)
                        {
                            van = true;
                            break;
                        }
                        else
                        {
                            van = false;
                        }
                        j++;
                    }

                    match.MatchId = int.Parse(sor_[0]);
                    match.labda.X = float.Parse(sor_[1]);
                    match.labda.Y = float.Parse(sor_[2]);
                    match.player1[0] = new Jatekos();
                    match.player1[0].X = float.Parse(sor_[3]);
                    match.player1[0].Y = float.Parse(sor_[4]);

                    match.player1[1] = new Jatekos();
                    match.player1[1].X = float.Parse(sor_[5]);
                    match.player1[1].Y = float.Parse(sor_[6]);

                    match.player1[2] = new Jatekos();
                    match.player1[2].X = float.Parse(sor_[7]);
                    match.player1[2].Y = float.Parse(sor_[8]);

                    match.player2[0] = new Jatekos();
                    match.player2[0].X = float.Parse(sor_[9]);
                    match.player2[0].Y = float.Parse(sor_[10]);

                    match.player2[1] = new Jatekos();
                    match.player2[1].X = float.Parse(sor_[11]);
                    match.player2[1].Y = float.Parse(sor_[12]);

                    match.player2[2] = new Jatekos();
                    match.player2[2].X = float.Parse(sor_[13]);
                    match.player2[2].Y = float.Parse(sor_[14]);

                    if (van)
                    {
                        MatchList.matchList[j] = match;
                    }
                    else
                    {
                        MatchList.matchList.Add(match);
                    }
                    
                }
            }
        }
    }
}