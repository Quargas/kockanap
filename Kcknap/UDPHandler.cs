﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Kcknap
{
    class UDPHandler
    {
        static UdpClient udpClient = new UdpClient();
        static IPEndPoint RemoteIpEndPoint = new IPEndPoint(IPAddress.Any, 0);

        public static UdpClient UdpClient
        {
            get
            {
                return udpClient;
            }
        }


        public static void sendMessage(string hostname, int port, string message_)
        {
            Byte[] message = Encoding.ASCII.GetBytes(message_);

            udpClient.Send(message, message.Length, hostname, port);



        }

    }
}
